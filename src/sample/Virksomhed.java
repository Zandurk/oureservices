package sample;

import javafx.collections.ObservableList;

/**
 * Created by Alex on 18-11-2015.
 */
public class Virksomhed {
    private String id;
    private String name;

    public Virksomhed(String navn, String id) {
        this.name = navn;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
