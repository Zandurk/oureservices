package sample;

import com.sun.org.apache.bcel.internal.generic.ALOAD;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class Controller {


public ObservableList<String> virksomhedsList = FXCollections.observableArrayList(
        "TDC","ISS","Dansk Kabel TV", "Bodum"
);

     @FXML
    ListView virksomhedsListe = new ListView<String>();

    @FXML
    TextField sogField;
    @FXML
    Label virksomhedsLabel, vaerdiLabel, klipBarLabel;
    @FXML
    ProgressBar klipBar;
    @FXML
    Button sendMail;

    private String vaerdi = "V�rdi: ";
    public void setVirksomhedsList(){
        virksomhedsListe.setItems(virksomhedsList);
    }
    public void getVirksomhed(String oldValue, String newValue){
        search(oldValue, newValue);
        virksomhedsListe.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String klip = "20%";
                klipBar.setProgress(0.2);
                klipBarLabel.setText(vaerdi + klip);
                virksomhedsLabel.setText(newValue);
                vaerdiLabel.setText(vaerdi + klip);

                if (newValue.equals("TDC")){
                    String klip1 = "50%";
                    klipBar.setProgress(0.5);
                    klipBarLabel.setText(vaerdi + klip1);
                    virksomhedsLabel.setText(newValue);
                    vaerdiLabel.setText(vaerdi + klip1);
                }
            }
        });

    }

    private void search(String oldValue, String newValue) {
        if ( oldValue != null && (newValue.length() < oldValue.length()) ) {
            // Restore the lists original set of entries
            // and start from the beginning
            virksomhedsListe.setItems(virksomhedsList);
        }

        // Change to upper case so that case is not an issue
        newValue = newValue.toUpperCase();

        // Filter out the entries that don't contain the entered text
        ObservableList<String> subentries = FXCollections.observableArrayList();
        for ( Object entry: virksomhedsListe.getItems() ) {
            String entryText = (String)entry;
            if ( entryText.toUpperCase().contains(newValue) ) {
                subentries.add(entryText);
            }
        }
        virksomhedsListe.setItems(subentries);
    }

    public void sogVirksomhed(Event event) {
        setVirksomhedsList();


        getVirksomhed(sogField.getText(), sogField.getText());

    }

    public void sendMail(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ikke implementeret i denne version");
        alert.setHeaderText("Funktionen er ikke implementeret endnu.");
        alert.setContentText("Da dette er en prototype er funktionen ikke implementeret endnu");
    }
}
