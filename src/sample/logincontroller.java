package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Alex on 18-11-2015.
 */
public class logincontroller {

    Main m = new Main();

    @FXML
    PasswordField password;
    @FXML
    TextField brugernavn;
    @FXML
    Button login, nytlogin;

    public void nytlogin(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ikke implementeret");
        alert.setHeaderText("Funktionen er ikke implementeret endnu");
        alert.setContentText("Da programmet er en prototype er denne funktion ikke implementeret endnu.");
        alert.showAndWait();
    }


    public void login(ActionEvent actionEvent) {
        String bn = brugernavn.getText();
        String pw = password.getText();
        if (bn.equals("OuroServices") && pw.equals("ouros")) {
            try {
                Stage s;
                m.changeScene(s = new Stage());
                ((Node)(actionEvent.getSource())).getScene().getWindow().hide();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Forkert kodeord eller brugernavn!");
            alert.setHeaderText("Kodeordet eller brugernavnet er forkert! Pr�v igen.");
            alert.showAndWait();
        }
    }
}
