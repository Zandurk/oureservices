package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    Stage primaryStage = new Stage();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root2 = FXMLLoader.load(getClass().getResource("login.fxml"));
        primaryStage.setTitle("Ouro Services Login");
        primaryStage.setScene(new Scene(root2, 590, 220));
        primaryStage.setResizable(false);
        primaryStage.show();

    }




    public static void main(String[] args) {
        launch(args);
    }

    public void changeScene(Stage primaryStage2) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage2.setTitle("Ouro Services");
        primaryStage2.setScene(new Scene(root, 600, 400));
        primaryStage2.setResizable(false);
        primaryStage2.show();
    }


}
